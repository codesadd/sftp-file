package com.codesadd.mc;

import java.io.FileInputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.security.PrivateKey;

import com.codesadd.mc.oauth.OAuth;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class McApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(McApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		String partnerId = "ptnr_BEeCrYJHh2BXTXPy_PEtp-8DBOo";
		String consumerKey = "U2_J1gHxLSF3wj9nNNWCnUrUqkToLRDWeqvyMZAiea48c8a0!6513bc5d7cd540c19767de65b1e386480000000000000000";
		URI uri = URI.create("https://sandbox.api.mastercard.com/send/static/v1/partners/"+partnerId+"/merchant/transfers/payment");
	
		String payload = "{\"merchant_payment_transfer\":{\"payment_type\":\"P2M\",\"amount\":\"120\",\"currency\":\"USD\",\"payment_origination_country\":\"IND\",\"sender_account_uri\":\"pan:5299920000000149;exp=2022-08;cvc=123\",\"transaction_local_date_time\":\"2017-09-22T13:22:11-05:30\",\"sender\":{\"first_name\":\"Fred\",\"middle_name\":\"Pete\",\"last_name\":\"Flinstone\",\"address\":{\"line1\":\"21Broadway\",\"line2\":\"ApartmentA-62\",\"city\":\"OFallon\",\"country_subdivision\":\"MO\",\"postal_code\":\"63368\",\"country\":\"USA\"},\"date_of_birth\":\"1994-05-21\",\"phone\":\"123456789\",\"email\":\"test123@abcmail.com\"},\"recipient_account_uri\":\"pan:5299920210000277;exp=2077-08;cvc=123\",\"recipient\":{\"merchant_category_code\":\"4829\",\"first_name\":\"Fred\",\"middle_name\":\"Pete\",\"last_name\":\"Flinstone\",\"address\":{\"line1\":\"1MainSt\",\"line2\":\"Apartment9\",\"city\":\"OFallon\",\"country_subdivision\":\"MO\",\"postal_code\":\"63368\",\"country\":\"USA\"},\"phone\":\"123456789\",\"email\":\"test123@abcmail.com\"},\"funding_source\":\"CASH\",\"reconciliation_data\":{\"custom_field\":[{\"name\":\"Paymentid\",\"value\":\"123\"},{\"name\":\"tranid\",\"value\":\"456\"},{\"name\":\"reference\",\"value\":\"789\"}]},\"participant\":{\"card_acceptor_name\":\"testmerchant\"},\"mastercard_assigned_id\":\"111111\",\"channel\":\"KIOSK\",\"device_id\":\"DEVICE-1234\",\"location\":\"state:MO\",\"participation_id\":\"Imparticipating1\",\"additional_message\":\"mymessage\",\"qr_data\":\"longrandomQRlimit237\",\"convenience_amount\":\"100\",\"convenience_indicator\":\"01\",\"transfer_reference\":\"MPQR_157427542617121\"}}";
		String method = "POST";
		Charset charset = Charset.forName("UTF-8");
		PrivateKey signingKey = gPrivateKey("keyalias", "keystorepassword");
		RestTemplate restTemplate = new RestTemplate();

		String authHeader = OAuth.getAuthorizationHeader(uri, method, payload, charset, consumerKey, signingKey);
		System.out.println(authHeader);

		HttpHeaders headers = new HttpHeaders();
		headers.set(HttpHeaders.AUTHORIZATION, authHeader);
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<>(payload, headers);
		Object response = restTemplate.exchange(uri, HttpMethod.POST, entity, Object.class);

		System.out.println(response);
	}

	public static PrivateKey gPrivateKey(String alias, String password) throws Exception {
		PrivateKey privateKey;
		KeyStore keyStore = KeyStore.getInstance("PKCS12");
		keyStore.load(new FileInputStream(ResourceUtils.getFile("classpath:OPTECH-sandbox.p12")), password.toCharArray());
		privateKey = (PrivateKey) keyStore.getKey(alias, password.toCharArray());

		if (privateKey == null) {
			throw new Exception("PrivateKey Null");
		}
		return privateKey;
	}



}
